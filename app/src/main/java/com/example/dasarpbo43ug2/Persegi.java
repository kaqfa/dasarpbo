package com.example.dasarpbo43ug2;

import android.util.Log;

public class Persegi extends BidangDatar{
    int panjang, lebar;

    public Persegi(int panjang, int lebar){
        this.namaBidang = "Persegi";
        this.panjang = panjang;
        this.lebar = lebar;
    }

    public Persegi(){
        this.namaBidang = "Persegi";
        this.panjang = 2;
        this.lebar = 1;
    }

    public Persegi(String panjang, String lebar){
        this.namaBidang = "Persegi";
        this.panjang = Integer.parseInt(panjang);
        this.lebar = Integer.parseInt(lebar);
    }

    public double getLuas(){
        luas = panjang * lebar;
        return luas;
    }

    public double getKeliling(){
        keliling = (2*panjang) + (2*lebar);
        return keliling;
    }
}
