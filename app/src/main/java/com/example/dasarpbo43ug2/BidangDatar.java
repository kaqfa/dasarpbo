package com.example.dasarpbo43ug2;

import android.util.Log;

public abstract class BidangDatar {
    public static final String TAG = "bidang";

    protected String namaBidang;
    protected double luas, keliling;

    abstract public double getLuas();

    abstract public double getKeliling();

    public void hitungBidang(){
        Log.d(TAG, "Nama Bidang: "+namaBidang);
        Log.d(TAG, "Luasnya: "+getLuas());
        Log.d(TAG, "Kelilingnya: "+getKeliling());
        Log.d(TAG, "=========================================");
    }
}
