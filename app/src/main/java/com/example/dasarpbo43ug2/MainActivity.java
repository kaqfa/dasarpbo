package com.example.dasarpbo43ug2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<BidangDatar> listBidang = new ArrayList<>();
        listBidang.add(new Persegi(4, 8));
        listBidang.add(new Lingkaran(5));
        listBidang.add(new Persegi());
        listBidang.add(new Persegi("6", "3"));
        listBidang.add(new Lingkaran(50));

        for(BidangDatar bidang: listBidang){
            bidang.hitungBidang();
        }
    }
}